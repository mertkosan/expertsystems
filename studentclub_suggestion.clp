;Newcomers Expert System
;This expert system recommmends university club to new coming university freshman students.
;This Clips file is written by mertkosan
;Some functionalities are taken from https://github.com/smarr/

;functions. (this functions are taken from https://github.com/smarr/

(deffunction ask-question (?question $?allowed-values)
   (printout t ?question)
   (bind ?answer (read))
   (if (lexemep ?answer) 
       then (bind ?answer (lowcase ?answer)))
   (while (not (member ?answer ?allowed-values)) do
      (printout t ?question)
      (bind ?answer (read))
      (if (lexemep ?answer) 
          then (bind ?answer (lowcase ?answer))))
   ?answer)

(deffunction yes-or-no-p (?question)
   (bind ?response (ask-question ?question yes no))
   (if (eq ?response yes)
       then yes 
       else no))

;Questions

(defrule determine-entertainment ""
   (not (_entartainment ?))
   (not (_suggested ?))
   =>
   (assert (_entartainment (yes-or-no-p "Do you think entertainment should be included in clubs (yes/no)? "))))
   
(defrule determine-sport ""
   (_entartainment yes)
   (not (_suggested ?))
   =>
   (assert (_sport (yes-or-no-p "Do you like spor activities (yes/no)? "))))

(defrule determine-politics ""
   (_entartainment no)
   (not (_suggested ?))   
   =>
   (assert (_politics (yes-or-no-p "Do you like politics & talking about politics (yes/no)? "))))
   
(defrule determine-outdoor ""
   (_sport yes)
   (not (_suggested ?))
   =>
   (assert (_outdoor (yes-or-no-p "Do you like outdoor activies (yes/no)? "))))
   
(defrule determine-nature ""
   (_sport no)
   (not (_suggested ?))
   =>
   (assert (_robotics (yes-or-no-p "Do you like science & robotics (yes/no)? "))))

(defrule determine-water-nature ""
   (_outdoor yes)
   (not (_suggested ?))
   =>
   (assert (_outdoor_activity (ask-question "Would you prefer water or nature sports (water/nature)? "
                                              water nature))))

(defrule determine-under-water ""
   (_outdoor_activity water)
   (not (_suggested ?))
   =>
   (assert (_under_water
               (yes-or-no-p "Would you like to be under water (yes/no)? "))))

(defrule determine-horses ""
   (_outdoor_activity nature)
   (not (_suggested ?))
   =>
   (assert (_horse
              (yes-or-no-p "Would you like to control horses (yes/no)? "))))

(defrule determine-own_robot ""
   (_robotics yes)
   (not (_suggested ?))
   =>
   (assert (_own_robot
              (yes-or-no-p "Would you like to make own robots (yes/no)? "))))

(defrule determine-games ""
   (_robotics no)
   (not (_suggested ?))
   =>
   (assert (_games
              (yes-or-no-p "Do you like games (yes/no)? "))))

(defrule determine-talk ""
   (_games no)
   (not (_suggested ?))
   =>
   (assert (_talk
              (yes-or-no-p "Do you like to talk (yes/no)? "))))

(defrule determine-competition ""
   (_politics yes)
   (not (_suggested ?))
   =>
   (assert (_competition
              (yes-or-no-p "Do you like competition (yes/no)? "))))

(defrule determine-ceo ""
   (_politics no)
   (not (_suggested ?))
   =>
   (assert (_ceo
              (yes-or-no-p "Do you wonder how to become CEO (yes/no)? "))))

(defrule determine-major ""
   (_ceo no)
   (not (_suggested ?))
   =>
   (assert (_major
              (ask-question "What would you like to study in University (art/engineering/economics)? "
                            art engineering economics))))

;Suggestions, Terminals

(defrule dance_suggestion ""
   (_outdoor no)
   (not (_suggested ?))
   =>
   (assert (_suggested "Dance Club.")))

(defrule scuba-diving-suggestion ""
   (_under_water yes)
   (not (_suggested ?))
   =>
   (assert (_suggested "Scuba Diving Club."))) 

(defrule sail_suggestion ""
   (_under_water no)
   (not (_suggested ?))
   =>
   (assert (_suggested "Sail Club.")))     

(defrule horse-suggestion ""
   (_horse yes)
   (not (_suggested ?))
   =>
   (assert (_suggested "Horse Riding Club.")))

(defrule nature-sport-suggestion ""
   (_horse no)
   (not (_suggested ?))
   =>
   (assert (_suggested "Nature Sports Club.")))

(defrule robotics-suggestion ""
   (_own_robot yes)
   (not (_suggested ?))
   =>
   (assert (_suggested "Robotics Club.")))

(defrule astro-suggestion ""
   (_own_robot no)
   (not (_suggested ?))
   =>
   (assert (_suggested "Astro Club.")))

(defrule frp-suggestion ""
   (_games yes)
   (not (_suggested ?))
   =>
   (assert (_suggested "FRP & Games Club.")))

(defrule radio-suggestion ""
   (_talk yes)
   (not (_suggested ?))
   =>
   (assert (_suggested "Radio Club.")))

(defrule photo-suggestion ""
   (_talk no)
   (not (_suggested ?))
   =>
   (assert (_suggested "Photography Club.")))

(defrule debate-suggestion ""
   (_competition yes)
   (not (_suggested ?))
   =>
   (assert (_suggested "Debate Club.")))

(defrule politics-suggestion ""
   (_competition no)
   (not (_suggested ?))
   =>
   (assert (_suggested "Politics Club.")))

(defrule ceo-suggestion ""
   (_ceo yes)
   (not (_suggested ?))
   =>
   (assert (_suggested "Talk with CEOs Club.")))

(defrule art-suggestion ""
   (_major art)
   (not (_suggested ?))
   =>
   (assert (_suggested "Art History & Mesuems Club.")))

(defrule engineering-suggestion ""
   (_major engineering)
   (not (_suggested ?))
   =>
   (assert (_suggested "Engineering Society Club.")))

(defrule economics-suggestion ""
   (_major economics)
   (not (_suggested ?))
   =>
   (assert (_suggested "Economy & Business Club.")))

;main and suggestion

(defrule main-start ""
  (declare (salience 5))
  =>
  (printout t crlf crlf)
  (printout t "The New Coming Student Club Suggestion Expert System")
  (printout t crlf crlf))

(defrule make-suggestion ""
  (declare (salience 5))
  (_suggested ?item)
  =>
  (printout t crlf crlf)
  (printout t "Suggested Club:")
  (printout t crlf crlf)
  (format t " %s%n%n%n" ?item))